# Sử dụng Haml (phần 1)

## Cài đặt môi trường

Cài đặt theo các bước sau:

1. Windows :satisfied:
2. [Git](http://git-scm.com/downloads)
3. [Source Tree](http://downloads.atlassian.com/software/sourcetree/windows/SourceTreeSetup_1.5.2.exe)
4. [Ruby](http://rubyinstaller.org/downloads/): chọn phiên bản 1.9.3(Khi cài đặt Ruby, nhớ check vào **Add Ruby executables to your PATH** ở cửa sổ cài đặt. Chọn đường dẫn không được chứa dấu cách, khuyến nghị nên cài vào thư mục gốc của ổ `C:` (Ví dụ như `C:\Ruby200-x64`)
5. [Devkit](http://rubyinstaller.org/downloads/): chọn phiên phản dành cho Ruby 1.9.3. Khi cài đặt, chọn đường dẫn không chứa dấu cách, khuyến nghị nên cài vào trong thư mục Ruby (ví dụ như `C:\Ruby193\dekit`). Xem thêm hướng dẫn tại https://github.com/oneclick/rubyinstaller/wiki/Development-Kit.
6. [Sublime Text](http://www.sublimetext.com/2). Sau khi cài xong:

    + Mở sublime lên và [cài package control](https://sublime.wbond.net/installation#st2)
    + Restart sublime, ấn `Ctrl-Shift-P`, gõ tìm lệnh `Package Control: Install Package` sau khi chờ fetch xong danh sách package và hiển thị hộp thoại nhập tên package, nhập `haml`, chọn *Haml: HAML Bundle for Textmate* để cài package hỗ trợ hiển thị file `haml`
    + Làm tương tự bước trên nhưng cho package *SCSS: The TextMate SCSS Official Bundle. Now Compatible with SublimeText2*.
    + Khởi động lại sublime
    
7. Mở cửa sổ Command Line của windows (`cmd`) lên và cài **theo thứ tự sau** (bước này cần có kết nối internet)

    + `gem install haml`
    + `gem install middleman`
    + `gem install eventmachine`. Tại bước này, nếu có lỗi là **bình thường**, giải quyết theo chỉ dẫn [tại đây](http://brettklamer.com/diversions/non-statistical/install-eventmachine-in-ruby-2-for-windows/).

Sau khi thực hiện các bước trên, trong máy tính sẽ có các chương trình sau

* Git
* Middleman
* Haml
* Scss
* Source Tree
* Sublime Text 2

## Thực hành

Sau khi đọc được file này, thực hiện các yêu cầu sau:

1. Clone repository này về máy
2. Tạo một branch mới có tên là **tên đăng nhập** của mình (tất cả các code trong tut này đều **không** được commit vào `master`
3. Chỉnh sửa file `source/index.haml` để hiển thị một trang *About me* giới thiệu về bản thân (nói tóm gọn là show hàng :smirk:):

    * Hình ảnh, css, javascript phải được tải về và đặt trong thư mục tương ứng.
    * Không dùng SCSS
    * Không hạn chế số lần commit, chú ý commit thường xuyên để dễ dàng quay lại nếu bị lỗi
    * Một lần nữa, các commit này phải được thực hiện trong branch mới tạo, **không** được thực hiện trong `master`
  
4. Chú ý build file để xem thử code có đúng ý của mình hay không.

Nếu gặp khó khăn, [tạo issue](https://bitbucket.org/mangoads-dev-team/haml-tut-1/issues?status=new&status=open) để mọi người có thể thảo luận và rút kinh nghiệm.

## Chú ý khi viết code

* Code **haml**, **scss** dùng `snake_case` (chữ thường, gạch dưới). 
* Code **javascript** dùng `camelCase` (chữ liền, viết hoa mỗi từ). Tên class sẽ viết hoa chữ cái đầu tiên, còn lại viết thường chữ cái đầu tiên
* Dùng tiếng Anh để đặt tên biến  
* Trong **haml** cùng **Ruby style**, không dùng **Html style**